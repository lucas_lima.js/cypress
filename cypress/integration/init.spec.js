/* eslint-disable no-undef */
describe("Form", () => {
    beforeEach(() => {
        cy.visit("/")
    });

    it("Auto focus on note input when open page", () => {
        cy.focused().should("have.class", "note-input")
    });

    it("Accepts input", () => {
        const text = "Some text";
        cy.get(".note-input")
            .type(text)
            .should("have.value", text);
    });

    it("Add new note", () => {
        const text = "New note";
        cy.get(".note-input")
            .type(text)
            .type("{enter}")
            .type(text)
            .type("{enter}")
            .get(".item-list")
            .should("have.length", 2)
            .get(".item-list")
            .first()
            .should("have.text", text)
            .click()
            .get(".item-list")
            .should("have.length", 1)
    });
})