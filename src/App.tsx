import React from "react";
import "./App.css";

import NewNoteForm from './components/NewNoteForm';
import ListNote from './components/ListNote';

function App() {
  return (
    <div className="container">
      <NewNoteForm />
      <ListNote />
    </div>
  );
}

export default App;
