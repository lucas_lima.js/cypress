import { nanoid } from "nanoid";

import { NoteStoreType } from "./types";

export function createNotesStore(): NoteStoreType {
    return {
        notes: JSON.parse(localStorage.getItem("notes") || "[]"),
        addNote(text) {
            this.notes = [
                {
                    id: nanoid(),
                    text,
                },
                ...this.notes
            ];
            this.persist();
        },
        removeNote(id) {
            this.notes = this.notes.filter((note) => note.id !== id);
            this.persist();
        },
        persist() {
            localStorage.setItem("notes", JSON.stringify(this.notes));
        },
    };
}
