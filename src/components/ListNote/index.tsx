import React from "react";
import { useObserver } from "mobx-react";
import "./style.css";

import { useNotesStore } from "../../store/notes/NotesContext";

const ListNote: React.FC = () => {
    const noteStore = useNotesStore();
    return useObserver(() => (
        <div className="container-list">
            {noteStore.notes.map((note) => (
                <div
                    key={note.id}
                    onClick={() => noteStore.removeNote(note.id)}
                    className="item-list"
                >
                    {note.text}
                </div>
            ))}
        </div>
    ));
};

export default ListNote;
