import React from "react";
import "./styles.css";

import { useNotesStore } from "../../store/notes/NotesContext";

const NewNoteForm: React.FC = () => {
    const [noteText, setNoteText] = React.useState("");
    const noteStore = useNotesStore();

    function addNewTask() {
        noteStore.addNote(noteText);
        setNoteText("");
    }

    return (
        <form
            className="form"
            onSubmit={(e) => {
                e.preventDefault();
                addNewTask();
            }}
        >
            <input
                type="text"
                placeholder="Some text"
                autoFocus
                value={noteText}
                onChange={(e) => setNoteText(e.target.value)}
                className="note-input"
            />
            <button type="submit">Add note</button>
        </form>
    );
};

export default NewNoteForm;
